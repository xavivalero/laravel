<?php

use App\Http\Controllers\OfertaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controlador1;
use App\Http\Controllers\FitxerController;
use App\Http\Controllers\EmpresaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [OfertaController::class, 'ofertasNoEnviadas']);

Route::get('/fitxa', [FitxerController::class, 'editUser']);
Route::post('/editUser', [FitxerController::class, 'updateUser']);
Route::post('/FitxerPujat', [FitxerController::class,'pujarFitxer']);

//emnpreses
Route::get('/empresa', [EmpresaController::class, 'showEmpresas']);

Route::get('/empresa/add', [EmpresaController::class, 'empresaAdd']);

Route::get('empresa/edit/{id}', [EmpresaController::class, 'editarEmpresa']);

Route::post('/addEmpresa', [EmpresaController::class, 'create']);

Route::post('/editEmpresa', [EmpresaController::class, 'updateEmpresa']);

//estudis
Route::get('/estudis', [Controlador1::class, 'mostrarEstudisCoordinador']);

Route::get('/estudis/add', [Controlador1::class, 'estudisAdd']);

Route::post('/addEstudio', [Controlador1::class, 'createEstudio']);

//empreses ofertes
Route::get('/empresa/oferta', [EmpresaController::class, 'showOfertes']);

Route::get('/empresa/oferta/add/{id}', [EmpresaController::class, 'ofertaAdd']);
Route::post('/addOferta', [EmpresaController::class, 'createOferta']);

Route::get('/empresa/oferta/edit/{id}', [EmpresaController::class, 'ofertaIdEdit']);
Route::post('/editOferta', [EmpresaController::class, 'editOferta']);

Route::get('/empresa/oferta/enviar', [EmpresaController::class, 'enviarOfertas']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


