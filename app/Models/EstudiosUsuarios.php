<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstudiosUsuarios extends Model
{
    use HasFactory;
    protected $table = "estudios_usuarios";
    protected $primaryKey = "id";
    protected $fillable = ["id", "idEstudio", "idUsuario", "añoPromocion"];
}
