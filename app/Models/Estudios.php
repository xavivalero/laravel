<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudios extends Model
{
    use HasFactory;
    protected $table = 'estudios';
    protected $primaryKey = 'idEstudio';
    protected $fillable = ['idEstudio', 'nombre'];

    public function ofertas()
    {
        return $this->belongsToMany(Ofertas::class, 'estudios_ofertas', 'idEstudio', 'idOferta')->withTimestamps();
    }

    public function usuarios() {
        return $this->belongsToMany(User::class, 'estudios_usuarios', 'idEstudio', 'idUsuario')->withTimestamps();

    }


}
