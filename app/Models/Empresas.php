<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    use HasFactory;
    protected $table = 'empresas';
    protected $primaryKey = 'idEmpresa';
    protected $fillable = ['idEmpresa', 'nombre', 'email'];

    public function ofertas() {

        return $this->hasMany(Ofertas::class, 'idEmpresa', 'idEmpresa');
    }
}
