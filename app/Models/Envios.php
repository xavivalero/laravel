<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Envios extends Model
{
    use HasFactory;
    protected $table = "envios";
    protected $fillable = ["idUsuario", "idOferta"];

    protected function setKeysForSaveQuery($query)
    {
        return $query->where('idUsuario', $this->getAttribute('idUsuario'))
            ->where('idOferta', $this->getAttribute('idOferta'));
    }
}
