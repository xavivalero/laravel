<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $primaryKey = 'idUsuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'apellido',
        'DNI',
        'trabaja',
        'telefono',
        'email',
        'password',
        'coordinador',
        'filePath'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function estudios() {
        return $this->belongsToMany(Estudios::class, 'estudios_usuarios', 'idUsuario', 'idEstudio')->withTimestamps();
    }

    public function ofertas() {
        return $this->belongsToMany(Ofertas::class, 'envios', 'idUsuario', 'idOferta')->withTimestamps();
    }

}
