<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstudiosOfertas extends Model
{
    use HasFactory;
    protected $table = "estudios_ofertas";
    protected $primaryKey = ["idEstudio", "idOferta"];
    protected $fillable = ["idEstudio", "idOferta"];
}
