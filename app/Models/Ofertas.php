<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ofertas extends Model
{
    use HasFactory;
    protected $table = 'ofertas';
    protected $primaryKey = 'idOferta';
    protected $fillable = ['idOferta', 'descripcion','enviada', 'idEmpresa'];

    public function estudios()
    {
        return $this->belongsToMany(Estudios::class, 'estudios_ofertas', 'idOferta', 'idEstudio')->withTimestamps();
    }

    public function empresas() {
        return $this->belongsTo(Empresas::class, 'idEmpresa', 'idEmpresa');
    }

    public function usuarios() {
        return $this->belongsToMany(User::class, 'envios', 'idOferta', 'idUsuario')->withTimestamps();
    }
}
