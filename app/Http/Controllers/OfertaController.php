<?php

namespace App\Http\Controllers;

use App\Models\Ofertas;
use Illuminate\Http\Request;

class OfertaController extends Controller
{
    public function ofertasNoEnviadas() {
        if(auth()->user() == null) {
            return view('/auth/login');
        }else{
            if(auth()->user()->coordinador == true){
                $ofertasNoEnviadas = Ofertas::select("*")
                    ->where("enviada", "=", 0)
                    ->get();
                return view('listarOfertas', compact('ofertasNoEnviadas'));
            }else{
                $ofertasNoEnviadas = Ofertas::select("*")
                    ->where("enviada", "=", 1)
                    ->get();
                return view('listarOfertas', compact('ofertasNoEnviadas'));
            }
        }
    }
}
