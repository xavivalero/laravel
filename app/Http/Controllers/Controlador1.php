<?php

namespace App\Http\Controllers;

use App\Models\Empresas;
use App\Models\Estudios;
use App\Models\Ofertas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Controlador1 extends Controller
{
    public function mostrarEstudisCoordinador()
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            $estudis = Estudios::all();


            /*$estudis = DB::table('estudios')
                ->select(
                'estudios.nombre'
                )
                ->join('estudios_usuarios', 'estudios.idEstudio','=','estudios_usuarios.idEstudio')
                ->where('estudios_usuarios.idUsuario', '=', auth()->user()->idUsuario)->get();*/
            return view('listarEstudios', compact('estudis'));
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    public function estudisAdd()
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            return view('estudis');
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    protected function createEstudio(Request $request)
    {
        Estudios::create([
            'nombre' => $request->get('nombre')
        ]);
        return redirect('/estudis');
    }

}
