<?php

namespace App\Http\Controllers;

use App\Models\Empresas;
use App\Models\Envios;
use App\Models\Ofertas;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Session;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    protected function create(Request $request)
    {
        Empresas::create([
            'nombre' => $request->get('nombre'),
            'email' => $request->get('email'),
        ]);
        return redirect('/empresa');
    }

    protected function createOferta(Request $request)
    {
        Ofertas::create([
            'descripcion' => $request->get('descripcion'),
            'enviada' => $request->get('enviada', 0),
            'idEmpresa' => $request->get('id'),
        ]);
        $todas = Ofertas::all();
        return view('listarTodasOfertas', compact('todas'));
    }

    public function updateEmpresa(Request $request) {
        DB::table('empresas')->where('idEmpresa', $request->get('id'))->update(['nombre' => $request->get('nombre'), 'email' => $request->get('email'), 'updated_at' => now(),]);
        return redirect('/empresa');
    }

    protected function editOferta(Request $request)
    {
        DB::table('ofertas')->where('idOferta', $request->get('id'))->update(['descripcion' => $request->get('descripcion') ,'enviada' => $request->get('enviada', 0), 'idEmpresa' => $request->get('idEmpresa')]);
        $todas = Ofertas::all();
        return view('listarTodasOfertas', compact('todas'));
    }

    public function showEmpresas()
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            $empresas = Empresas::all();
            /*$empresas = DB::table('empresas')
                ->select(
                    'empresas.nombre',
                    'empresas.email'
                )
                ->join('ofertas', 'ofertas.idEmpresa','=','empresas.idEmpresa')
                ->join('estudios_ofertas', 'ofertas.idOferta','=','estudios_ofertas.idOferta')
                ->join('estudios', 'estudios_ofertas.idEstudio','=','estudios.idEstudio')
                ->join('estudios_usuarios', 'estudios.IdEstudio','=','estudios_usuarios.idEstudio')
                ->where('estudios_usuarios.idUsuario', '=', auth()->user()->idUsuario)->get();*/
            return view('listarEmpresas', compact('empresas'));
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    public function showOfertes()
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            $todas = Ofertas::all();
            return view('listarTodasOfertas',compact('todas'));
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    public function empresaAdd()
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            return view('empresa');
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    public function editarEmpresa($id) {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            return view('editarEmpresa')->with('id', $id);
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }
    }

    public function ofertaAdd($id)
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            return view('oferta')->with('id', $id);
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }

    public function ofertaIdEdit($id)
    {
        if(auth()->user() != null && auth()->user()->coordinador == true){
            $empresas = Empresas::select("*")
                ->get();
            return view('editOferta', compact('empresas'))->with('id', $id);
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }
    }


    public function enviarOfertas()
    {

        if(auth()->user() != null && auth()->user()->coordinador == true){
            $titulados = User::select("*")
                ->where("trabaja", "=", 0)
                ->where("coordinador", "=", 0)
                ->get();
            $ofertasNoEnviadas = Ofertas::select("*")
                ->where("enviada", "=", 0)
                ->get();
            foreach ($titulados as $alumno){
                foreach ($ofertasNoEnviadas as $oferta){
                    Envios::create([
                        'idUsuario' => $alumno->idUsuario,
                        'idOferta' => $oferta->idOferta,
                    ]);
                }
            }

            DB::table('ofertas')->where('enviada', false)->update(['enviada' => true]);
            $info = Envios::all();
            return $info->toJson();
        }else{
            return 'Registrat com a coordinador per accedir a aquesta funcionalitat!';
        }

    }
}
