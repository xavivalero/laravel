<?php

namespace App\Http\Controllers;

use App\Models\Empresas;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class FitxerController extends Controller
{
    //

    public function pujarFitxer(Request $Request)
    {
        //validacion, si no la cumple peta
        $Request->validate([
            //el campo file es obligatorio, debe ser extension pdf xlx o csv, tamaño maximo 2MB
            'file' => 'required|mimes:pdf,xlx,csv|max:2048',
        ]);

        //nombre del fichero sacado a aprtir del time
        $fileName = time().auth()->user()->name.'.'.$Request->file->extension();

        //mover el fichero a la capeta public/uploads con el nombre generado anteriormente
        $Request->file->move(public_path('uploads'), $fileName);

        $fitxa = auth()->user();
        //$practica->contenido = $Request->contenido;
        //la nueva variable path$fileName
        $fitxa->filePath = $fileName;
        //$fitxa->filePath = public_path("uploads/" . $fileName);
        $fitxa->save();

        //puedes añadirle un campo a un redirect o return
        //return $fitxa->toJson();
        return redirect('/uploads/' . $fileName)->with('success','Fitxa entregada.')
            ->with('file',$fileName);
    }
    public function editUser()
    {
        if(auth()->user() != null && auth()->user()->coordinador == false){
            return view('fitxer');
        }else{
            return 'Registrat com a alumne titulat per accedir a aquesta funcionalitat!';
        }

    }

    public function updateUser(Request $request)
    {
        if($request->get('coordinador') == null){
            $coordinador = false;
        }else{
            $coordinador = true;
        }

        if($request->get('trabaja') == null){
            $trabaja = false;
        }else{
            $trabaja = true;
        }

        DB::table('users')->where('idUsuario', auth()->user()->idUsuario)->update(['name' => $request->get('name'), 'apellido' => $request->get('apellido'), 'DNI' => $request->get('DNI'), 'trabaja' => $trabaja, 'telefono' => $request->get('telefono'), 'email' => $request->get('email'), 'email_verified_At' => now(), 'coordinador' => $coordinador, 'updated_at' => now(),]);
        return 'Updated';

    }
}
