<?php

namespace Database\Seeders;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Ofertas;
use App\Models\Estudios;
use App\Models\Empresas;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class OfertasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $e1 = Estudios::create([
            'nombre' => 'DAM',

        ]);

        $u1 = User::create([
            'name' => 'Marc',
            'apellido' => 'Lomas',
            'DNI' => '47753391D',
            'telefono' => '722754723',
            'email' => 'marclobo12@outlook.com',
            'email_verified_at' => now(),
            'password' => Hash::make('1237'),
            'coordinador' => true,
        ]);

        $u2 = User::create([
            'name' => 'Xavi',
            'apellido' => 'Valero',
            'DNI' => '48270502B',
            'telefono' => '672288408',
            'email' => 'xavivalero2002@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('1234'),
            'coordinador' => false,
        ]);

        $em1 = Empresas::create([
                'nombre' => 'Nexiad',
                'email' => 'nexiad@nexiad.cat'
        ]);

        $em2 = Empresas::create([
            'nombre' => 'CRM',
            'email' => 'CRM@CRM.cat'
        ]);

        $o1 = Ofertas::create([
            'descripcion' => 'programador',
            'enviada' =>  true,
            'idEmpresa' => 1,
        ]);

        $o2 = Ofertas::create([
            'descripcion' => 'developer',
            'enviada' =>  false,
            'idEmpresa' => 1,
        ]);

        $e1->ofertas()->attach($o1);
        $e1->usuarios()->attach($u1);
        $e1->usuarios()->attach($u2);
        $o1->usuarios()->attach($u1);

        DB::table('estudios_usuarios')->where('añoPromocion', null)->update(['añoPromocion' => 2022]);

    }
}
