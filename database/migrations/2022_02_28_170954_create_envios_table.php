<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnviosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('envios', function (Blueprint $table) {
            $table->bigInteger("idUsuario")->unsigned();
            $table->foreign("idUsuario")->references("idUsuario")->on("users")->onDelete("cascade");
            $table->bigInteger("idOferta")->unsigned();
            $table->foreign("idOferta")->references("idOferta")->on("ofertas")->onDelete("cascade");
            $table->primary(["idUsuario","idOferta"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('envios');
    }
}
