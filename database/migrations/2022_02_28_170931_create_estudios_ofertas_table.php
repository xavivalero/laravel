<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosOfertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios_ofertas', function (Blueprint $table) {
            $table->bigInteger("idEstudio")->unsigned();
            $table->foreign("idEstudio")->references("idEstudio")->on("estudios")->onDelete("cascade");
            $table->bigInteger("idOferta")->unsigned();
            $table->foreign("idOferta")->references("idOferta")->on("ofertas")->onDelete("cascade");
            $table->primary(["idEstudio","idOferta"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EstudiosOfertas');
    }
}
