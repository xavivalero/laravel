<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios_usuarios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("idEstudio")->unsigned();
            $table->foreign("idEstudio")->references("idEstudio")->on("estudios")->onDelete("cascade");
            $table->bigInteger("idUsuario")->unsigned();
            $table->foreign("idUsuario")->references("idUsuario")->on("users")->onDelete("cascade");
            $table->bigInteger("añoPromocion")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EstudiosUsuarios');
    }
}
