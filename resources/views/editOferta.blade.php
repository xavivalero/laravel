@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Editar oferta') }}</div>

                    <div class="card-body">
                        <form method="post" action="{{url('editOferta')}}">
                            @csrf

                            <div class="row mb-3">
                                <label for="descripcion" class="col-md-4 col-form-label text-md-end">{{ __('Descripción') }}</label>

                                <div class="col-md-6">
                                    <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" required autofocus>

                                    @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="form group row" >
                                <label for="enviada" class="col-md-4 form-check-label text-md-end">{{ __('Enviada') }}</label>

                                <div class="col-md-6 text-md-center">
                                    <input type="checkbox" class="form-check-input" name="enviada" value="1">
                                </div>

                            </div>


                            <div class="row mb-3">
                                <label for="idEmpresa" class="col-md-4 form-check-label text-md-end">{{ __('Empresa') }}</label>

                                <div class="col-md-6">
                                    <select name="idEmpresa" id="idEmpresa">
                                        @foreach($empresas as $element)
                                            <option value={{$element->idEmpresa}}>{{$element->nombre}}</option>
                                        @endforeach
                                    </select>
                                    @error('idEmpresa')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="row mb-3">

                                <div class="col-md-6">
                                    <input id="id" type="hidden" class="form-control @error('id') is-invalid @enderror" name="id" value="{{$id}}" required autofocus>

                                    @error('id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>

                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Añadir') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
