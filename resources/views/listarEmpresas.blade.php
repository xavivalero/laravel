@extends('layouts.app')

@section('content')
        <h1>LLista d'empreses</h1>
    @if(count($empresas) > 0)
        <ul>
            @foreach($empresas as $element)
                <li>Nom: {{$element->nombre}}, Email: {{$element->email}}</li>
            @endforeach
        </ul>
    @else
        <li>No hi ha elements</li>
    @endif

@endsection
