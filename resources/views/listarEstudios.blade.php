@extends('layouts.app')

@section('content')
    <h1>LLista de tots els estudis</h1>
    @if(count($estudis) > 0)
        <ul>
            @foreach($estudis as $element)
                <li>Nom: {{$element->nombre}}</li>
            @endforeach
        </ul>
    @else
        <li>No hi ha elements</li>
    @endif

@endsection
