@extends('layouts.app')

@section('content')
@if(auth()->user()->coordinador == true)
    <h1>Llistar ofertes pendents d'enviar</h1>
@else
    <h1>Llistar ofertes enviades</h1>
@endif
@if(count($ofertasNoEnviadas) > 0)
    <ul>
        @foreach($ofertasNoEnviadas as $element)
            <li>Descripcio: {{$element->descripcion}}, Empresa: {{$element->empresas->nombre}}</li>
        @endforeach
    </ul>
@else
    <li>No hi ha elements</li>
@endif

@endsection
