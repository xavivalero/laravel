<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>La meva fitxa</title>
</head>
<body>
@include('layouts.app')

@section('content')
    <div class="container">
        <h2>Ficha</h2>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>ERROR!</strong> T'has equivocat!!!
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="/FitxerPujat" enctype="multipart/form-data">

            <div class="form-group">
                <textarea name="contenido" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <input type="file" name="file" class="form-control">
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-primary">Entregar Ficha</button>
            </div>
            {{ csrf_field() }}
        </form>
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if (auth()->user()->filePath != null)
            <a href="http://localhost:8000/uploads/{{ auth()->user()->filePath }}">Descargar ficha</a>
        @endif


    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-header">{{ __('Editar Usuario') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('editUser') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ auth()->user()->name }}" autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="apellido" class="col-md-4 col-form-label text-md-end">{{ __('Apellido') }}</label>

                            <div class="col-md-6">
                                <input id="apellido" type="text" class="form-control @error('apellido') is-invalid @enderror" name="apellido" value="{{ auth()->user()->apellido }}" autocomplete="apellido" autofocus>

                                @error('apellido')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="DNI" class="col-md-4 col-form-label text-md-end">{{ __('DNI') }}</label>

                            <div class="col-md-6">
                                <input id="DNI" type="text" class="form-control @error('DNI') is-invalid @enderror" name="DNI" value="{{ auth()->user()->DNI }}" autocomplete="DNI" autofocus>

                                @error('DNI')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form group row" >
                            <label for="trabaja" class="col-md-4 form-check-label text-md-end">{{ __('Trabaja') }}</label>

                            <div class="col-md-1 text-md-center">
                                <input type="checkbox" class="form-check-input" name="trabaja" value="{{ auth()->user()->trabaja }}"  @if(auth()->user()->trabaja) checked @endif>
                            </div>

                        </div>

                        <div class="row mb-3">
                            <label for="telefono" class="col-md-4 col-form-label text-md-end">{{ __('Telefono') }}</label>

                            <div class="col-md-6">
                                <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ auth()->user()->telefono }}" autocomplete="telefono" autofocus>

                                @error('telefono')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ auth()->user()->email }}" autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form group row" >
                            <label for="coordinador" class="col-md-4 form-check-label text-md-end">{{ __('Coordinador?') }}</label>

                            <div class="col-md-1 text-md-center">
                                <input type="checkbox" class="form-check-input" name="coordinador" value="{{ auth()->user()->coordinador }}"  @if(auth()->user()->coordinador) checked @endif>
                            </div>

                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Editar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    </form>
                </div>
            </div>
        </div>
    </div>
@show
</body>
</html>
