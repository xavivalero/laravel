@extends('layouts.app')

@section('content')
        <h1>Llista de totes ofertes</h1>
    @if(count($todas) > 0)
        <ul>
            @foreach($todas as $element)
                <li>Descripcio: {{$element->descripcion}}, Empresa: {{$element->empresas->nombre}}</li>
            @endforeach
        </ul>
    @else
        <li>No hi ha elements</li>
    @endif

@endsection

